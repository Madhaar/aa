//
//  DataManager.swift
//  AA
//
//  Created by Manjinder Singh on 06/04/2019.
//  Copyright © 2019 Manjinder Singh. All rights reserved.
//

import Foundation


//let sharedDataManager = DataManager(sharedCoreDataManager)
protocol DataManagerProtocol{
    func getData(onlineCompletionHanlder:@escaping (WeatherCodable?) -> Void)
}

class DataManager<NP: NetworkProtocol>: DataManagerProtocol {
    
    let networkManager  : NP
    
    init(_ networkManager: NP) {
        self.networkManager = networkManager
    }
}

//Post
extension DataManager{
    
    func getData(onlineCompletionHanlder:@escaping (WeatherCodable?) -> Void){
        
        self.networkManager.getData(WeatherAPI.city(name: "London")) {(weather: WeatherCodable?, error) in
            onlineCompletionHanlder(weather)
        }
    }
}
