//
//  NetworkManager.swift
//  AA
//
//  Created by Manjinder Singh on 09/04/2019.
//  Copyright © 2019 Manjinder Singh. All rights reserved.
//

import Foundation


protocol NetworkProtocol{
    associatedtype T
    func getData<T: Codable>(_ route: WeatherAPI, callback: @escaping (T?, _ errorString: String?) -> Void)
}

struct NetworkManager: NetworkProtocol{
   
    typealias T = Codable
    
    static let environment : NetworkEnvironment = .production
    
    var router: Router<WeatherAPI>
    
    init(_ session: URLSessionProtocol) {
        router = Router<WeatherAPI>(currentSession: session)
    }
    
    fileprivate func handleNetworkResponse(_ response: HTTPURLResponse) -> Result<String>{
        switch response.statusCode {
        case 200...299: return .success
        case 401...500: return .failure(NetworkResponse.authenticationError.rawValue)
        case 501...599: return .failure(NetworkResponse.badRequest.rawValue)
        case 600: return .failure(NetworkResponse.outdated.rawValue)
        default: return .failure(NetworkResponse.failed.rawValue)
        }
    }
    
    func getData<T: Codable>(_ route: WeatherAPI, callback: @escaping (T?, String?) -> Void) {
    
        router.request(route) { (data, response, error) in
            
            if error != nil {
                callback(nil, "Network error!")
            }
            
            if let response = response as? HTTPURLResponse {
                let result = self.handleNetworkResponse(response)
                switch result {
                case .success:
                    guard let responseData = data else {
                        callback(nil, NetworkResponse.noData.rawValue)
                        return
                    }
                    
                    do {
                        let apiResponse = try JSONDecoder().decode(T.self, from: responseData)
                        //  print(apiResponse)
                        callback(apiResponse, nil)
                    }catch {
                        print(error)
                        callback(nil, NetworkResponse.unableToDecode.rawValue)
                    }
                case .failure(let networkFailureError):
                    callback(nil, networkFailureError)
                }
            }
        }
    }
}

enum Result<String>{
    case success
    case failure(String)
}

enum NetworkResponse:String {
    case success
    case authenticationError = "Please authenticate first"
    case badRequest = "Bad request"
    case outdated = "The url you requested is outdated."
    case failed = "Network request failed."
    case noData = "No Data."
    case unableToDecode = "Decode failed."
}
