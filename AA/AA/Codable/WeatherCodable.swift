//
//  WeatherCodable.swift
//  AA
//
//  Created by Manjinder Singh on 10/04/2019.
//  Copyright © 2019 Manjinder Singh. All rights reserved.
//

import Foundation

struct WeatherCodable: Codable{
    let cod: String
    let message: Double
    let cnt: Int
    let list: [List]
    let city: City
    
   init(from decoder: Decoder) throws {
        let container   = try decoder.container(keyedBy: CodingKeys.self)
        cod             = try container.decode(String.self, forKey: .cod)
        message         = try container.decode(Double.self, forKey: .message)
        cnt             = try container.decode(Int.self, forKey: .cnt)
        list            = try container.decode([List].self, forKey: .list)
        city            = try container.decode(City.self, forKey: .city)
    }
}

struct List: Codable{
    let dt: Double
    let dt_txt: String
    let main: Main
    let weather: [weather]
    let clouds: Clouds
    let wind: Wind
    let sys: Sys
}

struct Main:Codable{
    let temp: Float
    let temp_min: Float
    let temp_max: Float
    let pressure: Double
    let sea_level: Double
    let grnd_level: Double
    let humidity: Double
    let temp_kf: Double
}

struct weather:Codable {
    let id: Int
    let main: String
    let description: String
    let icon: String
}

struct Clouds: Codable{
    let all: Int
}

struct Wind: Codable{
    let speed: Double
    let deg: Double
}

struct Sys: Codable{
    let pod: String
}

struct City: Codable{
    let id: Int
    let name: String
    let coord: Coord
    let country: String
}

struct Coord: Codable{
    let lat: Double
    let lon: Double
}
