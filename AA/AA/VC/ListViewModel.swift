//
//  WeatherViewModel.swift
//  AA
//
//  Created by Manjinder Singh on 14/04/2019.
//  Copyright © 2019 Manjinder Singh. All rights reserved.
//

import Foundation

class ListViewModel {
    
    let list: List
    
    init(with list: List) {
        self.list = list
    }
    
    var time: String{
        
        let compo = self.list.dt_txt.components(separatedBy: " ")
        if compo.count > 1{
            return String(self.list.dt_txt.components(separatedBy: " ").last!.prefix(5))
        }
        else{
            return ""
        }
    }
    
    var temp: String{
        let stringTemp = String(self.list.main.temp)
        return "\(stringTemp.prefix(5))°"
    }
    
    var highestTemp: String{
        
        return "\(String(list.main.temp_max).prefix(5))°"
    }
    var lowestTemp: String{
        return "\(String(list.main.temp_min).prefix(5))°"
    }
    
    var speed: String{
        return "Speed \(list.wind.speed)"
    }
    
    var windDeg: String{
        return "\(String(list.wind.deg).prefix(5))°"
    }
    
}
